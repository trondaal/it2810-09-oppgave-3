import React, { Component } from 'react';
import './style.css';
import Button from '../Button';

class LoginButton extends Component {

  constructor(props) {
    super(props);
    this.className = this.props.className;
    this.onClick = this.props.onClick;
    this.onCancel = this.props.onCancel;
    this.formIsActive = this.props.formIsActive;
  }

  render() {
    return(
      <Button
        hollow
        className={this.className}
        // Change button function according to if the login form is visible or not
        onClick={this.formIsActive() ? this.onCancel : this.onClick}
      >
        Login
      </Button>
    )
  }

}

export default LoginButton;
