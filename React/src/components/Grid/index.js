import React, { PropTypes } from 'react';

import  './style.css';

const Grid = (props) => {
  /**
   * Grid: A flexbox based Grid component.
   *
   * @params {string} alignContent, alignItems, justifyContent
   * -- CSS parameters. Used to define inline styling.
   *
   * @param {bool} paddedChildren
   * -- Specifies if the Grid needs padding between its children
   * -- This happens by pushing the 'paddedChildren' class, which gets styled by Grid/style.css
   *
   * @param {bool} wrap
   * -- If true, sets the flex-box parameter 'flex-wrap' to 'wrap'.
   * -- This happens by pushing the 'wrap' class, which gets styled by Grid/style.css
   *
   * @param {node} children
   * -- A list of child elements for this column.
   */

  const {
    alignContent,
    alignItems,
    justifyContent,
    paddedChildren,
    wrap,
    ...other,
  } = props;

  // Locally scoped classes
  const classes = ['grid'];
  if (paddedChildren) classes.push('padded-children');
  if (wrap) classes.push('grid-wrap');

  // Inline styling :(
  const style = {};
  if (justifyContent) style.justifyContent = justifyContent;
  if (alignItems) style.alignItems = alignItems;
  if (alignContent) style.alignContent = alignContent;

  return (
    <div {...other} className={classes.join(' ')} style={style}>
      {props.children}
    </div>
  );
};

Grid.propTypes = {
  alignItems: PropTypes.string,
  alignContent: PropTypes.string,
  justifyContent: PropTypes.string,
  wrap: PropTypes.bool,
  paddedChildren: PropTypes.bool,
  children: PropTypes.node,
};
export default Grid;
