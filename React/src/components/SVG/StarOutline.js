import React from 'react';
/**
 * @returns svg image with fill attribute to override colors.
 */
const StarOutline = (props) => {
	return (
    <svg
      version="1.1"
      id="StarOutline"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 20 20"
      width="18px"
      height="18px"
    >
      <path fill={props.color} d="M18.8,8.022h-6.413L10,1.3L7.611,8.022H1.199l5.232,3.947L4.56,18.898L10,14.744l5.438,4.154l-1.869-6.929L18.8,8.022zM10,12.783l-3.014,2.5l1.243-3.562l-2.851-2.3L8.9,9.522l1.1-4.04l1.099,4.04l3.521-0.101l-2.851,2.3l1.243,3.562L10,12.783z" />
    </svg>
	);
};

export default StarOutline;
