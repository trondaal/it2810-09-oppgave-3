
import React from 'react';
/**
 * @returns svg image with fill attribute to override colors.
 */
const FemaleSign = (props) => {
	return (
    <svg
      version="1.1"
      id="FemaleSign"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="30px"
      height="30px"
      viewBox="0 0 400 400" preserveAspectRatio="none"
    >
        <ellipse fill="none" strokeWidth="35" stroke="#000000" cx="133" cy="130.785" rx="115.7302" ry="113.5152"/>
        <path d="M109.47,257V303.505H56.2729V345.722H109.47V400H156.53V345.722H207.681V303.505H156.53V257Z"/>
    </svg>

	);
};

export default FemaleSign;
