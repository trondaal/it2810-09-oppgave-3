import React from 'react';
/**
 * @returns svg image with fill attribute to override colors.
 */
const StarHalf = (props) => {
	return (
    <svg
      viewBox="0 0 18 18"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width="18px"
      height="18px"
    >
      <path
        fill={props.color}
        d="M17.8,7.022 L11.387,7.022 L9,0.3 L6.611,7.022 L0.199,7.022 L5.431,10.969 L3.56,17.898 L9,13.744 L14.438,17.898 L12.569,10.969 L17.8,7.022 Z M9.02319336,4.50628662 L10.0979614,8.52722168 L13.6104736,8.42474365 L10.7723389,10.7213745 L12.0105591,14.2852173 L9.01745605,11.7957764 L9.02319336,4.50628662 Z"
      />
    </svg>
	);
};

export default StarHalf;
