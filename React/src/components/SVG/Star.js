import React from 'react';
/**
 * @returns svg image with fill attribute to override colors.
 */
const Star = (props) => {
	return (
    <svg
      version="1.1"
      id="Star"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 20 20"
      width="18px"
      height="18px"
    >
      <path fill={props.color} d="M10,1.3l2.388,6.722H18.8l-5.232,3.948l1.871,6.928L10,14.744l-5.438,4.154l1.87-6.928L1.199,8.022h6.412L10,1.3z" />
    </svg>
	);
};

export default Star;
