import React, { PropTypes } from 'react';
import classnames from 'classnames';
import './style.css';


const ButtonGroup = (props) => (
  <div className={classnames('button-group', props.className)}>
    {props.children}
  </div>
);

ButtonGroup.propTypes = {
  children: PropTypes.node,
};
export default ButtonGroup;
