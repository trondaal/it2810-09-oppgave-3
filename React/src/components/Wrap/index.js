import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import './style.css';

/**
* Wrap
* -- Creates a component to create margin on left and right side of the content, with screen adjustments.
*
* @param {node} children
* -- A list of child elements for this column.
*/
class Wrap extends Component {
  render() {
    const { className, overrideMaxWidth, children } = this.props;

    // Inline styles
    const style = {};
    if (overrideMaxWidth) style.maxWidth = overrideMaxWidth;

    return (
      <div
        className={classnames('wrap', className)}
        style={style}
      >
        {children}
      </div>
    );
  }
}

Wrap.propTypes = {
  className: PropTypes.string,
  overrideMaxWidth: PropTypes.string,
  children: PropTypes.node,
};
export default Wrap;
