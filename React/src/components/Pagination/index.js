import React, { Component } from 'react';
import './style.css';
import Button from '../../components/Button';
import ButtonGroup from '../../components/ButtonGroup';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
    };

    this.changePage = this.changePage.bind(this);
    this.incrementOrDecrementPage = this.incrementOrDecrementPage.bind(this);
  }

  changePage(e) {
    const newPage = parseInt(e.target.getAttribute('data-pn'), 10);

    this.props.changePage(newPage);
    this.setState({ currentPage: newPage });
  }

  incrementOrDecrementPage(e) {
    const direction = (e.target.getAttribute('data-direction') === '+' ? 1 : -1);
    const newPage = this.state.currentPage + (direction);
    this.setState({ currentPage: newPage });
    this.props.changePage(newPage);
  }

  renderPageButtons(pageAmount) {
    const pages = [];

    for (let i = 1; i <= pageAmount; i++) {
      let inlineStyle = {};
      if (this.state.currentPage === i - 1) {
        inlineStyle = { backgroundColor: '#eee' };
      }

      pages.push(
        <Button
          hollow
          key={`page-${i}`}
          data-pn={i-1}
          onClick={this.changePage}
          style={inlineStyle}
        >
          {i}
        </Button>
      );
    }

    return pages;
  }

  render() {
    const { pageAmount } = this.props;

    return (
      <div className="pagination">
        <ButtonGroup>
          <Button
            hollow
            data-direction="-"
            onClick={this.incrementOrDecrementPage}
            disabled={this.state.currentPage === 0}
            className="page-nav-arrow"
          >
            &lt;
          </Button>

          {this.renderPageButtons(pageAmount)}

          <Button
            hollow
            data-direction="+"
            onClick={this.incrementOrDecrementPage}
            disabled={this.state.currentPage === (pageAmount - 1)}
            className="page-nav-arrow"
          >
            &gt;
          </Button>
        </ButtonGroup>
      </div>
    );
  }
}

export default Pagination;
