import React, { Component, PropTypes } from 'react';

import './style.css';


class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.initialValue || '',
    };
  }

  handleChange(e) {
    this.setState({
      value: e.target.value,
    });
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  }

  render() {
    const { ...other } = this.props;
    delete other.onChange;
    delete other.initialValue;

    return (
      <input
        type="text"
        {...other}
        value={this.state.value}
        className="input"
        onChange={(e) => this.handleChange(e)}
      />
    );
  }
}

Input.propTypes = {
  onChange: PropTypes.func,
  initialValue: PropTypes.string,
  icon: PropTypes.string,
};
export default Input;
