import React, { Component } from 'react';
import './style.css';
import Button from '../Button';
import Input from '../Input';
import api from '../../api';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      credentials: {
        user_name: '',
        password: ''
      },
      loginInsteadOfRegister: true,
      errorMessage: null,
      successMessage: null,
      create: {
        user_name: '',
        password: '',
        password2: '',
        email: '',
        first_name: '',
        last_name: '',
        gender: ''
      },
      match: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.validateCreate = this.validateCreate.bind(this);
    this.onChangeCreate = this.onChangeCreate.bind(this);
    this.onCancel = this.props.onCancel;

    this.handleLoginInsteadOfRegister = this.handleLoginInsteadOfRegister.bind(this);
  }


  onChange(e) {
    const field = e.target.name;
    const creds = this.state.credentials;
    creds[field] = e.target.value;

    return this.setState({credentials: creds})
  }

  onChangeCreate(e) {
    const field = e.target.name;
    const creates = this.state.create;

    creates[field] = e.target.value;

    return this.setState({create: creates})
  }

  onSubmit(e) {
    e.preventDefault();
    api.login(this.state.credentials)
    .then(token => {
      if (token.success) {
        this.props.onSuccess(token.token);
      }
      else {
        this.setState({errorMessage: token.message});
      }
    });
  }

  validateCreate(e){
    //TODO: Check for numbers in name and proper email etc.
    e.preventDefault();
    let valid = true;
    if (this.state.create.password === this.state.create.password2)
    {
      this.setState({match: ''});
    }
    else {
      this.setState({match: 'wrong'})
      this.setState({errorMessage: 'Passwords do not match.'});
      valid = false;
    }

    for (let property in this.state.create) {
      if (this.state.create.hasOwnProperty(property)) {
        //Non react way, but less work
        if (this.state.create[property] === '') {
          valid = false;
          document.getElementsByName(property)[0].className += ' error';

          //Own check for radio button
          if (property === 'gender') {
            document.getElementsByClassName('input-label')[0].style.color = 'red';
          }
          this.setState({errorMessage: 'There are missing required fields!'});
        }

      }
    }

    if (valid) {
      api.createUser(this.state.create)
      .then(res => {
        if (res.success) {
          this.setState({loginInsteadOfRegister: true,
            successMessage: 'Successfully created user!',
            create: {
              user_name: '',
              password: '',
              password2: '',
              email: '',
              first_name: '',
              last_name: '',
              gender: ''
            }
          });
        }
        else {
          this.setState({errorMessage: res.message});
        }
      });
    }
  }

  handleLoginInsteadOfRegister(e) {
    const flag = !this.state.loginInsteadOfRegister;
    return this.setState({loginInsteadOfRegister: flag});
  }

  renderLoginForm() {
    const { credentials } = this.state;

    return (
      <form>
        <h2>Login</h2>
        <Input
          name="user_name"
          placeholder="Username"
          value={credentials.user_name}
          onChange={this.onChange}
        />
        <Input
          type="password"
          name="password"
          placeholder="Password"
          onChange={this.onChange}
          value={credentials.password}
        />
        <Button large primary fluid onClick={this.onSubmit}>Login</Button>
        <Button fluid onClick={this.handleLoginInsteadOfRegister}>
          Create new account
        </Button>
      </form>
    );
  }

  renderRegistrationForm() {
    const { create, match } = this.state;

    return (
      <form method="post">
        <h2>Register</h2>
        <Input
          type="text"
          name="user_name"
          placeholder="Username"
          onChange={this.onChangeCreate}
          value={create.user_name}
          />
        <Input
          type="password"
          name="password"
          placeholder="Password"
          onChange={this.onChangeCreate}
          value={create.password}
          />
        <Input
          type="password"
          name="password2"
          placeholder="Password again"
          onChange={this.onChangeCreate}
          value={create.password2}
          className={match}
          />
        <Input
          type="email"
          name="email"
          placeholder="Email address"
          onChange={this.onChangeCreate}
          value={create.email}
          />
        <div className="divider"></div>
        <Input
          type="text"
          name="first_name"
          placeholder="First name"
          onChange={this.onChangeCreate}
          value={create.first_name}
          />
        <Input
          type="text"
          name="last_name"
          placeholder="Last name"
          onChange={this.onChangeCreate}
          value={create.last_name}
          />
        <div className="input-label">Gender</div>
        <ul>
          <li className="radio-container">
            <input type="radio" id="male-gender" name="gender" value="male" onChange={this.onChangeCreate}/>
            <label htmlFor="male-gender"> Male</label>

            <div className="check"><div className="inside"></div></div>
          </li>
          <li className="radio-container">
            <input type="radio" id="female-gender" name="gender" value="female" onChange={this.onChangeCreate} />
            <label htmlFor="female-gender"> Female</label>

            <div className="check"><div className="inside"></div></div>
          </li>
        </ul>
        <Button primary large fluid onClick={this.validateCreate}>Create account</Button>
        <Button fluid onClick={this.handleLoginInsteadOfRegister}>
          Already registered? Sign In
        </Button>
      </form>
    );
  }

  render() {
    const {
      errorMessage,
      loginInsteadOfRegister,
      successMessage,
    } = this.state;

    return (
      <div className="modal">
        <div className="modal-content">
          <span className="modal-close" onClick={this.onCancel}>×</span>
          {loginInsteadOfRegister ? this.renderLoginForm() : this.renderRegistrationForm()}
          {errorMessage && <div className="error-message">{errorMessage}</div>}
          {successMessage && <div className="success-message">{successMessage}</div>}
        </div>
      </div>
    );
  }
}

export default LoginForm;
