import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import './style.css';

// Checks if an URL is external (meaning pointing to another site) or not.
const isExternalUrl = (url) => {
  const domainRegex = /https?:\/\/((?:[\w\d-]+\.)+[\w\d]{2,})/i;
  const domain = (fullUrl) => {
    const regexRes = domainRegex.exec(fullUrl);
    return regexRes ? regexRes[1] : null;
  };

  return domain(location.href) !== domain(url);
};

/**
 * Button
 * link (string, optional): Create a link styled as a button
 * hollow (boolean, optional): Remove button background color, and add a colored border
 * fluid (boolean, optional): Make button full width
 * primary (boolean, optional): Blue color
 * success (boolean, optional): Green color
 * warning (boolean, optional): Warning color
 * alert (boolean, optional): Alert color
 */
const Button = (props) => {
  const {
    alert,
    fluid,
    hollow,
    icon,
    large,
    link,
    noStyle,
    primary,
    success,
    warning,
    ...other,
  } = props;
  delete other.styles;

  // Set classes based on values in props
  const classes = ['button'];
  if (alert) classes.push('alert');
  if (fluid) classes.push('fluid');
  if (hollow) classes.push('hollow');
  if (icon) classes.push('icon');
  if (large) classes.push('large');
  if (noStyle) classes.push('no-style');
  if (primary) classes.push('primary');
  if (success) classes.push('success');
  if (warning) classes.push('warning');

  // If a prop.link and the link is external, render regular a-tag
  if (props.link && isExternalUrl(props.link)) {
    return (
      <a href={props.link} className={classes.join(' ')}>
        {props.children}
      </a>
    );
  }
  // Else if link and link is internal, render Redux router Link
  if (props.link) {
    return (
      <Link {...other} to={link} className={classes.join(' ')}>
        {props.children}
      </Link>
    );
  }
  // Else, render a normal button
  return (
    <button {...other} className={classes.join(' ')} >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  hollow: PropTypes.bool,
  fluid: PropTypes.bool,
  primary: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  alert: PropTypes.bool,
  noStyle: PropTypes.bool,
  link: PropTypes.string,
  large: PropTypes.bool,
  children: PropTypes.node.isRequired,
};
export default Button;
