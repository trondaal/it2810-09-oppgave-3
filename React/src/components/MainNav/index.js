import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import Wrap from '../Wrap';
import './style.css';
import logo from './logo.svg';
import { API_URL } from '../../constants';
import Button from '../Button';

/**
 * MainNav
 * -- class main-nav creates the navigation bar
 * -- class nav-wrap uses the imported row component to create a margin on the right and left side, including some
 * other components.
 * -- class brand uses the imported Link component to create a link to the homepage using the "/" path.
 * -- class logo uses the logo.svg file as the logo f the page. The color of paths in the svg file is overrided with the
 * "fill" attribute.
 */
class MainNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileButtonIsClicked: false,
    };
    this.isProfileButtonClicked = this.isProfileButtonClicked.bind(this);
    this.handleProfileButtonClick = this.handleProfileButtonClick.bind(this);
  }

  isProfileButtonClicked() {
    return this.state.profileButtonIsClicked;
  }
  handleProfileButtonClick() {
    const flag = !this.state.profileButtonIsClicked;
    this.setState({profileButtonIsClicked: flag});
  }

  render() {
    const { onLogout, loginButton, user } = this.props;

    return (
      <nav className="main-nav">
        <Wrap className="nav-wrap">
          <Link to="/" className="brand">
            <img src={logo} alt="Logo" className="logo"/>
            <h2 className="brand-text">Ticket resale</h2>
          </Link>
          {loginButton}
          {user &&
            <div className="profile">
              <Link to="/profile">
                <img onClick={this.handleProfileButtonClick}
                  src={`${API_URL}/img/${user[0].image}`}
                  alt="Profile"
                  className="card-seller-image profile-img"
                />
              </Link>
              <Button hollow link="/" onClick={onLogout}>Logout</Button>
            </div>
          }
        </Wrap>
      </nav>
    );
  }
}

MainNav.propTypes = {
  loginButton: PropTypes.node,
};

export default MainNav;
