import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './style.css';
import StarHalf from '../../components/SVG/StarHalf';
import Star from '../../components/SVG/Star';
import StarOutline from '../../components/SVG/StarOutline';


const Five = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <Star />
    <Star />
    <Star />
  </div>
);

const FourHalf = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <Star />
    <Star />
    <StarHalf />
  </div>
);

const Four = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <Star />
    <Star />
    <StarOutline />
  </div>
);

const ThreeHalf = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <Star />
    <StarHalf />
    <StarOutline />
  </div>
);

const Three = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <Star />
    <StarOutline />
    <StarOutline />
  </div>
);

const TwoHalf = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <StarHalf />
    <StarOutline />
    <StarOutline />
  </div>
);

const Two = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <Star />
    <StarOutline />
    <StarOutline />
    <StarOutline />
  </div>
);

const OneHalf = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <StarHalf />
    <StarOutline />
    <StarOutline />
    <StarOutline />
  </div>
);

const One = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <Star />
    <StarOutline />
    <StarOutline />
    <StarOutline />
    <StarOutline />
  </div>
);

const Half = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <StarHalf />
    <StarOutline />
    <StarOutline />
    <StarOutline />
    <StarOutline />
  </div>
);

const Zero = ({className}) => (
  <div className={classnames('star-rating-wrapper', className)}>
    <StarOutline />
    <StarOutline />
    <StarOutline />
    <StarOutline />
    <StarOutline />
  </div>
);

const StarRating = ({ rating, className }) => {
    /**
     * components Five, FourHalf, Four, ThreeHalf, Three, TwoHalf, Two, OneHalf, One, Half, Zero
     * are all used to within the StarRating component to determine which and how many svg images we want to return,
     * from star.js and starHalf.js.
     *
     * @param {string} className
     * @param {number} rating
     * -- a required number to determine which component to return.
     */
  if (rating > 4.75) return Five({className});
  else if (rating > 4.25) return FourHalf({className});
  else if (rating > 3.75) return Four({className});
  else if (rating > 3.25) return ThreeHalf({className});
  else if (rating > 2.75) return Three({className});
  else if (rating > 2.25) return TwoHalf({className});
  else if (rating > 1.75) return Two({className});
  else if (rating > 1.25) return OneHalf({className});
  else if (rating > 0.75) return One({className});
  else if (rating > 0.25) return Half({className});
  else return Zero({className});
};

StarRating.propTypes = {
  rating: PropTypes.number.isRequired,
  className: PropTypes.string,
};
export default StarRating;
