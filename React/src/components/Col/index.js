import React, { PropTypes } from 'react';

import './style.css';

const Col = (props) => {
  /**
   * Col: A grid column component
   * NB! To work it MUST be wrapped in a <Grid> component.
   *
   * @params {string} small, medium, large
   * -- Defines values (1-12) to indicate how much column space one element should use
   *      (can be used separately, i.e. both 'small=1' and 'large=4' is allowed)
   * -- (e.g. '1' = 1/12 = 8.333%;  '12' = 12/12 = 100%)
   *
   * @params {string} show, hide
   * -- NOTE: Reserved for future use
   * -- Specifies if some elements should be shown or hidden for certain screen sizes
   * -- (e.g. 'show="small-only"';  'hide="medium"')
   *
   * @param {bool} padded
   * -- Specifies if each column should have padding. Defaults to 'true' if used.
   *
   * @param {node} children
   * -- A list of child elements for this column.
   */

  const {
    hide,
    large,
    medium,
    padded,
    show,
    small,
    ...other,
  } = props;

  // Set classes based on values in props
  const classes = [];
  if (small) classes.push(`small-${small}`);
  if (medium) classes.push(`medium-${medium}`);
  if (large) classes.push(`large-${large}`);
  if (hide) classes.push(`hide-for-${hide}`);
  if (show) classes.push(`show-for-${show}`);
  if (padded) classes.push('padded');

  return (
    <div {...other} className={classes.join(' ')}>
      {props.children}
    </div>
  );
};

Col.propTypes = {
  small: PropTypes.string,
  medium: PropTypes.string,
  large: PropTypes.string,
  hide: PropTypes.string,
  show: PropTypes.string,
  padded: PropTypes.bool,
  children: PropTypes.node,
};
export default Col;
