import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import './style.css';


class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.checked || false,
    };
  }

  handleClick() {
    ReactDOM.findDOMNode(this.refs.checkbox).focus();
    this.setState({
      checked: !this.state.checked,
    });
    if (this.props.onClick) {
      this.props.onClick(!this.state.checked);
    }
  }

  render() {
    const { label, ...other } = this.props;

    return (
      <div className="checkbox" onClick={() => this.handleClick()}>
        <input
          {...other}
          type="checkbox"
          checked={this.state.checked}
          tabIndex="0"
          className="hidden"
          ref="checkbox"
        />
        <label>{label}</label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  onClick: PropTypes.func,
  label: PropTypes.string,
  checked: PropTypes.bool,
};
export default Checkbox;
