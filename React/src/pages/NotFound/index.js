import React from 'react';

import './style.css';

import MainNav from '../../components/MainNav';

const NotFound = () => (
  <div>
    <MainNav />
    <h1>
      404 <small>Not Found :(</small>
    </h1>
  </div>
);


export default NotFound;
