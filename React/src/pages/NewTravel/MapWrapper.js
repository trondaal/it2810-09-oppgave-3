import React from 'react'
import ReactDOM from 'react-dom'
import Map, { Marker } from 'google-maps-react'

import Input from '../../components/Input';

const Contents = React.createClass({
  getInitialState() {
    return {
      place: null,
      position: null
    }
  },

  onSubmit: function(e) {
    e.preventDefault();
  },

  componentDidMount: function() {
    this.renderAutoComplete();
  },

  componentDidUpdate(prevProps) {
    const { map } = this.props;
    if (map !== prevProps.map) {
      this.renderAutoComplete();
    }
  },

  renderAutoComplete: function() {
    const {google, map} = this.props;

    if (!google || !map) return;

    const aref = this.refs.autocomplete;
    const node = ReactDOM.findDOMNode(aref);
    var autocomplete = new google.maps.places.Autocomplete(node);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }

      this.setState({
        place: place,
        position: place.geometry.location
      })
      this.props.updateCoords(this.state.position);
    })
  },

  render: function() {
    const props = this.props;
    const {position} = this.state;
    return (
      <div>
        <div>
          <form onSubmit={this.onSubmit}>
            <label htmlFor="google-autocomplete">Where is the destination?</label>
            <Input
              id="google-autocomplete"
              ref="autocomplete"
              placeholder="Enter a location"
            />
            <input
              hidden
              type='submit'
              value='Go' />
          </form>
          <div>
            {position &&
            <input type="hidden" value={position} />
            }
          </div>
        </div>
        <div>
          <Map className="form-map" {...props}
              containerStyle={{
                position: 'relative',
                height: '400px',
                width: '100%'
              }}
              center={this.state.position}
              centerAroundCurrentLocation={false}>
                <Marker position={this.state.position} />
          </Map>
        </div>
      </div>
    )
  }
})

const MapWrapper = React.createClass({
  render: function() {
    const props = this.props;
    //const {google} = this.props;

    return (
      <Map google={window.google}
          className={'map'}
          containerStyle={{
            position: 'relative',
            height: '490px',
            width: '100%'
          }}
          visible={false}>
            <Contents {...props} />
      </Map>
    );
  }
})

export default MapWrapper;
