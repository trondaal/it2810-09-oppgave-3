import React, { Component } from 'react';
import cookie from 'react-cookie';
import { GoogleApiWrapper } from 'google-maps-react';
import api from '../../api';
import './style.css';
import MainNav from '../../components/MainNav';
import Wrap from '../../components/Wrap';
import Input from '../../components/Input';
import MapWrapper from './MapWrapper';

var jwtDecode = require('jwt-decode');

class NewTravel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        sellerID: '',
        name: '',
        description: '',
        price: '',
        hotel: false,
        roundtrip: false,
        destination_lat: '',
        destination_lng: ''
      },
      token: cookie.load('token'),
      loggedIn: false,
      activeTab: 0,
      fileMessage: 'Choose a file...',
      errorMessage: null,
      tabClasses: ['tab-button finished', 'tab-button'],
    }

    this.onChange = this.onChange.bind(this);
    this.updateCoords = this.updateCoords.bind(this);
    this.onImageSelected = this.onImageSelected.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeTab = this.changeTab.bind(this);

  }

  handleSubmit(e) {
    const inputs = this.state.inputs;
    var valid = true;
    let error = '';
    // TODO - MAKE JSX
    if (inputs.name === '') {
      valid = false;
      error += 'You must enter the name of the destination. ';
    }
    if (inputs.description === '') {
      valid = false;
      error += 'You must enter a description of the trip. ';
    }
    if (inputs.price === '') {
      valid = false;
      error += 'You must enter the price of the trip. ';
    }
    else if (isNaN(inputs.price)) {
      valid = false;
      error += 'Price must be in numbers.';
    }
    if (document.getElementById('file-image').files[0] == null) {
      valid = false;
      error += 'You must upload an image representing the destination. ';
    }
    if (inputs.destination_lat === '') {
      valid = false;
      error += 'You must choose where the destinaton is. ';
    }
    if (valid) {
        api.createTravel(this.state.inputs, document.getElementById('file-image').files[0])
        .then(res => {
            if (res.success) {
                window.location.href = "/profile";
            }
            else {
                this.setState({errorMessage: res.message});
            }
        });
    }
    else {
      this.setState({errorMessage: error, activeTab: 1});
    }
  }

  componentDidMount() {
    if (this.state.token != null) {
      var decoded = jwtDecode(this.state.token);
      var time = new Date() / 1000;
      if (time > decoded.exp) {
          cookie.remove('token');
      }
      else {
        decoded = decoded._doc;
        var person = {
          id: decoded.id,
          user_name: decoded.user_name,
          first_name: decoded.first_name,
          last_name: decoded.last_name,
          email: decoded.email,
          gender: decoded.gender,
          rating: decoded.rating,
          image: decoded.image,
          password: decoded.password,
          admin: decoded.admin,
        };
        //console.log(person);
        let temp = this.state.inputs;
        temp.sellerID = person.id;
        this.setState({loggedIn: person,
                       inputs: temp});
      }

    }
  }



  onImageSelected(e){
    e.preventDefault();
    var fileName = '';
    fileName = e.target.value.split( '\\' ).pop();
    if (fileName.length > 13) {
      fileName = fileName.substring(0, 12) + '...';
    }
    if( fileName )
      this.setState({fileMessage: fileName});
  }


  changeTab(e) {
    // Treat array in state as immutable, thus copy and edit
    let copyArr = this.state.tabClasses.slice();
    if (e.target.className === 'next') {
      copyArr[this.state.activeTab+1] = 'tab-button finished';
      // setState is async, so have to callback function
      this.setState({tabClasses: copyArr, activeTab: this.state.activeTab + 1}, () => {
        if (this.state.activeTab === 2) {
          this.handleSubmit(e);
        }
      });
    }
    else {
      copyArr[this.state.activeTab] = 'tab-button';
      this.setState({tabClasses: copyArr, activeTab: this.state.activeTab - 1});
    }


    if (this.state.activeTab === 1) {
      this.setState({buttonText: 'Publish'})
    }
  }

  updateCoords(position) {
    let temp = this.state.inputs;
    temp.destination_lat = position.lat();
    temp.destination_lng = position.lng();

    this.setState({inputs: temp});
  }

  onChange(event) {
    const field = event.target.name;
    const creds = this.state.inputs;
    if (field === 'hotel' || field === 'roundtrip') {
      creds[field] = event.target.checked;
    }
    else {
      creds[field] = event.target.value;
    }

    this.setState({inputs: creds})
  }

  render() {

    return (
      <div>
        <MainNav />
        <Wrap overrideMaxWidth="600px">
          <div className="tab-menu">
            <div className="tab-container">
              <div className={this.state.tabClasses[0] + ' tab-button-one'}>1 / Destination</div>
            </div>
            <div className="tab-container">
              <div className={this.state.tabClasses[1] + ' tab-button-two'}>2 / More</div>
            </div>
          </div>
          {this.state.errorMessage && <div className="error-message">{this.state.errorMessage}</div>}
          <div className="tabs">
            <div className={this.state.activeTab === 0 ? 'tab-one form-tab' : 'hidden'}>
              <MapWrapper updateCoords={this.updateCoords} google={window.google} />
            </div>
            <div className={this.state.activeTab === 1 ? 'tab-two form-tab' : 'hidden'}>
              <label htmlFor="location">Location</label>
              <Input
                type="text"
                name="name"
                placeholder="e.g. London, Turkey, Japan"
                value={this.state.inputs.name}
                onChange={this.onChange} />

              <label htmlFor="description">Description</label>
              <textarea
                id="form-description"
                name="description"
                value={this.state.inputs.description}
                onChange={this.onChange}>
              </textarea>
              <div className="row ">
                <div className="col c1">
                <label htmlFor="price">Price</label>
                <div className="price-container">
                  <Input
                    id="price"
                    name="price"
                    value={this.state.inputs.price}
                    onChange={(e) => this.onChange(e)} />
                  <label htmlFor="price" id="price-affix" className="currency">kr</label>
                </div>
                </div>
                <div className="col c2">
                  <div className="row">
                    <label htmlFor="roundtrip" className="form-check">Roundtrip
                      <Input
                        type="checkbox"
                        name="roundtrip"
                        value={this.state.inputs.roundtrip}
                        onChange={this.onChange}
                        id="roundtrip" />
                      <div className="checky"></div>
                    </label>
                  </div>
                  <div className="row">
                    <label htmlFor="hotel-included" className="form-check">Hotel included
                      <Input
                        name="hotel"
                        type="checkbox"
                        value={this.state.inputs.hotel}
                        onChange={this.onChange}
                        id="hotel-included" />
                      <div className="checky"></div>
                    </label>
                  </div>
                </div>
              </div>
              <label>Image</label>
              <input
                accept="image/*"
                type="file"
                id="file-image"
                className="input-image"
                name="image"
                onChange={this.onImageSelected} />
              <label htmlFor="file-image">{this.state.fileMessage}</label>
            </div>
            <div className={this.state.activeTab === 2 ? 'tab-three form-tab' : 'hidden'}>
            yo
            </div>
          <div className="tab-nav">
            <button hidden={this.state.activeTab === 0} onClick={(e) => this.changeTab(e)} className="back">Back</button>
            <button hidden={this.state.activeTab === 2} onClick={(e) => this.changeTab(e)} className="next">{this.state.activeTab === 0 ? 'Next':'Publish'}</button>
          </div>
          </div>
        </Wrap>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBcINV-zMCe7f8fpEeapFL4RNjd5ZaKdBM'
})(NewTravel);
