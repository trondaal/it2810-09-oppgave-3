import React from 'react';
import { Link } from 'react-router';

import './style.css';

const renderRoundtripText = (roundtrip) => (roundtrip ? "Round trip" : "One way");
const renderHotelIncludedText = (hotel) => (hotel ? "Hotel included" : "Hotel not included");

const CardView = (props) => {
  const { travels } = props;


  return (
    <table className="selectable">
      <thead>
        <tr>
          <th>Destination</th>
          <th>Price</th>
          <th>Type</th>
          <th>Other</th>
        </tr>
      </thead>
      <tbody>
      {travels.map(travel => (
        <tr key={travel.id}>
          <td><Link to={`travels/${travel.id}`}>{travel.name}</Link></td>
          <td><Link to={`travels/${travel.id}`}>{travel.price} kr</Link></td>
          <td><Link to={`travels/${travel.id}`}>{renderRoundtripText(travel.roundtrip)}</Link></td>
          <td><Link to={`travels/${travel.id}`}>{renderHotelIncludedText(travel.hotel)}</Link></td>
        </tr>
      ))}
      </tbody>
    </table>
  );
}

export default CardView;
