import React from 'react';

import './style.css';
import Grid from '../../components/Grid';
import Col from '../../components/Col';
import TravelCard from './TravelCard.js';

const CardView = (props) => {
  const { travels, sellers } = props;

  return (
    <Grid wrap paddedChildren>
      {travels.map(travel =>
        <Col padded large="4" medium="6" small="12" key={travel.id}>
          <TravelCard
            travel={travel}
            seller={sellers.find(seller => seller.id === travel.sellerID)}
          />
        </Col>
      )}
    </Grid>
  );
}

export default CardView;
