import React, { Component } from 'react';
import Map, { GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';

import Roundtrip from '../../components/SVG/Roundtrip';
import Building from '../../components/SVG/Building';

class WithMarkers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
    };
  }

  onMarkerClick(props, marker, e) {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });
  }

  onInfoWindowClose() {
    this.setState({
      showingInfoWindow: false,
      activeMarker: null,
    })
  }

  onMapClicked(props) {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null,
      })
    }
  }
  renderRoundtripText(roundtrip) {
    return (roundtrip ? "Round trip" : "One way");
  }

  renderHotelIncludedText(hotel) {
    return (hotel ? "Hotel included" : "Hotel not included");
  }

  render() {
    if (!this.props.loaded) {
      return <div>Loading...</div>
    }

    const { google, travels } = this.props;
    const { activeMarker, showingInfoWindow, selectedPlace } = this.state;

    return (
      <Map google={google}
        style={{ maxWidth: '1200px', height: '500px', display: 'relative' }}
        className={'map'}
        zoom={3}
        initialCenter={{lat: 40.9829888, lng: 28.810442500000022}}
        onClick={(props) => this.onMapClicked(props)}
      >
        {travels.map(travel => (
          <Marker
            onClick={(props, marker, e) => this.onMarkerClick(props, marker, e)}
            name={travel.name}
            travelID={travel.id}
            price={travel.price}
            hotel={travel.hotel}
            roundtrip={travel.roundtrip}
            position={{lat: travel.destination_lat, lng: travel.destination_lng}}
            key={travel.id}
          />
        ))}

        <InfoWindow
          marker={activeMarker}
          visible={showingInfoWindow}
          onClose={this.onInfoWindowClose}
        >
          <a href={`travels/${selectedPlace.travelID}`} className="info-window-wrapper">
            <h1>{selectedPlace.name}</h1>
            <p className="price">{selectedPlace.price} <span className="currency">kr</span></p>
            <hr className="card-hr" />
            <p className="card-info"><Roundtrip color="#999" /> {this.renderRoundtripText(selectedPlace.roundtrip)}</p>
            <p className="card-info"><Building color="#999" /> {this.renderHotelIncludedText(selectedPlace.hotel)}</p>
          </a>
        </InfoWindow>
      </Map>
    );
  }
};

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBcINV-zMCe7f8fpEeapFL4RNjd5ZaKdBM'
})(WithMarkers)
