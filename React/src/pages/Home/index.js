import React, { Component } from 'react';
import cookie from 'react-cookie';
import api from '../../api';
import './style.css';
import MainNav from '../../components/MainNav';
import Pagination from '../../components/Pagination';
import Input from '../../components/Input';
import Select from '../../components/Select';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';
import ButtonGroup from '../../components/ButtonGroup';
import Wrap from '../../components/Wrap';
import LoginForm from '../../components/LoginForm';
import LoginButton from '../../components/LoginButton';
var jwtDecode = require('jwt-decode');
import MapView from './MapView.js';
import CardView from './CardView.js';
import ListView from './ListView.js';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      travels: [],
      sellers: [],
      showLoginForm: false,
      token: cookie.load('token'),
      loggedIn: false,
      select: "min_max",
      filterHotel: false,
      filterRoundtrip: false,
      currentIndex: 0,
      searchTerm: '',
    };
    this.TRAVELS_PER_PAGE = 6;

    this.handleLoginButtonClick = this.handleLoginButtonClick.bind(this);
    this.handleLoginCancel = this.handleLoginCancel.bind(this);
    this.formIsActive = this.formIsActive.bind(this);
    this.changePage = this.changePage.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  componentDidMount() {
    api.getTravels()
      .then(travels => { this.setState({ travels })});

    api.getSellers()
      .then(sellers => { this.setState({ sellers })});
    if (this.state.token != null) {
      var decoded = jwtDecode(this.state.token);
      var time = new Date() / 1000;
      if (time > decoded.exp) {
          cookie.remove('token');
      }
      else {
        decoded = decoded._doc;
        var person = [{
          id: decoded.id,
          user_name: decoded.user_name,
          first_name: decoded.first_name,
          last_name: decoded.last_name,
          email: decoded.email,
          gender: decoded.gender,
          rating: decoded.rating,
          image: decoded.image,
          password: decoded.password,
          admin: decoded.admin,
        }];
        this.setState({loggedIn: person});
      }

    }
  }

  handleLoginButtonClick() {
    this.setState({showLoginForm: true});
  }

  handleLoginCancel() {
    this.setState({showLoginForm: false});
  }

  onLogin(token) {
    this.setState({ token });
    cookie.save('token', token);
    window.location.href = '/';
  }

  onLogout() {
    cookie.remove('token');
    this.setState({token: null, loggedIn: false});
  }

  formIsActive() {
    return this.state.showLoginForm;
  }

  handleSortChange(option) {
    this.setState({ select: option.value });
  }

  handleHotelIncludedChange(checked){
    this.setState({ filterHotel: checked, currentIndex: 0 });
  }
  changePage(i) {
    this.setState({currentIndex: i * this.TRAVELS_PER_PAGE });
  }

  handleRoundtripChange(checked){
    this.setState({ filterRoundtrip: checked, currentIndex: 0});
  }

  onSearch(e) {
    this.setState({searchTerm: e.target.value});
    if (e.target.value.length > 1) {
      this.setState({currentIndex: 0});
    }
  }

  sortTravels(travels) {
    const { select } = this.state;
    switch (select) {
      case "alphabetic":
        return travels.sort((a, b) => a.name.localeCompare(b.name));
      case "min_max":
        return travels.sort((a, b) => a.price - b.price);
      case "max_min":
        return travels.sort((a, b) => b.price - a.price);
      default:
        return travels;
    }
  }

  filterTravels(travels){
    const { filterRoundtrip, filterHotel, searchTerm } = this.state;
    let filteredTravels = travels;

    if (filterRoundtrip) {
      filteredTravels = filteredTravels.filter(travel => travel.roundtrip);
    }
    if (filterHotel) {
      filteredTravels = filteredTravels.filter(travel => travel.hotel);
    }
    if (searchTerm !== '' && searchTerm.length > 1) {
      let matches = [];
      for (let i = filteredTravels.length; i--; ){
        if (filteredTravels[i].name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ) {
            matches.push(filteredTravels[i]);
        }
      }
      filteredTravels = matches;
    }
    
    return filteredTravels;
  }

  renderControlPanel({ select, currentURL }) {
    return (
      <div className="control-panel">
        <div className="filter-wrapper">
          <Input
            placeholder="Search locations"
            className="filter-search"
            value={this.state.searchTerm}
            onChange={this.onSearch}
            id="search-input"
            name="search"
          />
          <div className="filters">
            <Checkbox onClick={(checked) => this.handleHotelIncludedChange(checked)} label="Hotel included" />
            <Checkbox onClick={(checked) => this.handleRoundtripChange(checked)} label="Round trip" />
          </div>
        </div>
        <div className="view-wrapper">
          <ButtonGroup>
            <Button link="/" hollow activeStyle={{ backgroundColor: '#eee' }}>
              <i className="fa fa-th-large"></i> Card
            </Button>
            <Button link="/list" hollow activeStyle={{ backgroundColor: '#eee' }}>
              <i className="fa fa-bars"></i> List
            </Button>
            <Button link="/map" hollow activeStyle={{ backgroundColor: '#eee' }}>
              <i className="fa fa-map"></i> Map
            </Button>
          </ButtonGroup>

          {!currentURL.match('/map') &&
            <Select
              value={select}
              options={
              [
                {value:'alphabetic', label: 'Alphabetic'},
                {value:'min_max', label: 'Price: Low to high'},
                {value:'max_min', label: 'Price: High to low'}
              ]}
              onChange={(option) => this.handleSortChange(option)}
              className="sort"
            />
          }
        </div>
      </div>
    );
}

  render() {
    const { travels, sellers, select, currentIndex } = this.state;

    // Display nothing if the data from the API server isn't fetched yet
    if (travels.length === 0 || sellers.length === 0) {
      return null;
    }

    const currentURL = this.props.location.pathname;
    const filteredTravels = this.filterTravels(travels);
    const sortedTravels = this.sortTravels(filteredTravels);
    const NUMBER_OF_PAGES =  Math.ceil(sortedTravels.length / this.TRAVELS_PER_PAGE);
    let visibleTravels = sortedTravels;
    let showPagination = false;

    if (currentURL === '/' || currentURL.match('/list')) {
      showPagination = true;
      visibleTravels = visibleTravels.slice(currentIndex, currentIndex + this.TRAVELS_PER_PAGE);
    }

    let travelView = <CardView travels={visibleTravels} sellers={sellers} />;
    if (currentURL.match('/map')) {
      travelView = <MapView travels={visibleTravels} />;
    } else if (currentURL.match('/list')) {
      travelView = <ListView travels={visibleTravels} />;
    }

    return (
      <div>
        <MainNav
          loginButton={!this.state.loggedIn && <LoginButton
            onClick={this.handleLoginButtonClick}
            onCancel={this.handleLoginCancel}
            formIsActive={this.formIsActive}
            className="nav-item nav-button login-button"
          />}
          user={this.state.loggedIn}
          onLogout={this.onLogout}
        />
        <Wrap>
          {this.renderControlPanel({ select, currentURL })}
          {travelView}
          { showPagination && <Pagination changePage={this.changePage} pageAmount={NUMBER_OF_PAGES} />}
        </Wrap>
        {this.state.showLoginForm && <LoginForm onSuccess={this.onLogin.bind(this)} onCancel={this.handleLoginCancel}/>}
      </div>
    );
  }
}

export default Home;
