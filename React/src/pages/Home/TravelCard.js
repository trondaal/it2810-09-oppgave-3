import React, { Component } from 'react';
import { Link } from 'react-router';

import { API_URL } from '../../constants';
import './style.css';
import Roundtrip from '../../components/SVG/Roundtrip';
import Building from '../../components/SVG/Building';
import StarRating from '../../components/StarRating';

class Travel extends Component {

  renderRoundtripText(roundtrip){
    return roundtrip ? "Round trip" : "One way";
  }

  renderHotelText(hotel) {
    return hotel ? "Hotel included" : "Hotel not included";
  }

  render() {
    const { travel, seller } = this.props;

    return (
      <Link to={`travels/${travel.id}`} className="card">
        <div className="card-image">
          <img src={`${API_URL}/img/${travel.image}`} alt="" />
        </div>
        <div className="name-and-price-wrapper">
          <h2>{travel.name}</h2>
          <p className="price">{travel.price} <span className="currency">kr</span></p>
          <div className="card-seller">
            <img src={`${API_URL}/img/${seller.image}`} alt="" className="card-seller-image" />
            <StarRating rating={seller.rating} className="card-rating" />
          </div>
        </div>
        <hr className="card-hr" />
        <p className="card-info"><Roundtrip color="#999" /> {this.renderRoundtripText(travel.roundtrip)}</p>
        <p className="card-info"><Building color="#999" /> {this.renderHotelText(travel.hotel)}</p>
      </Link>
    );
  }
}




export default Travel;
