import React, { Component } from 'react';

import './style.css';
import MainNav from '../../components/MainNav';
import api from '../../api';
import Wrap from '../../components/Wrap';

import { API_URL } from '../../constants';
import Roundtrip from '../../components/SVG/Roundtrip';
import Building from '../../components/SVG/Building';
import Grid from '../../components/Grid';
import Col from '../../components/Col';


class TravelDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      travel: {},
      seller: {},
    }
  }

  componentDidMount() {
    const travelID = parseInt(this.props.params.travelID, 10);

    api.getTravel(travelID) // Get the travel with the specified ID
      .then(travel => {
        this.setState({ travel });
        api.getSeller(travel.sellerID) // Then get the seller with the ID specified in the "travel object"
          .then(seller => this.setState({ seller }))
      });
  }

  renderRoundtripText(roundtrip){
    return roundtrip ? "Round trip" : "One way";
  }

  renderHotelText(hotel) {
    return hotel ? "Hotel included" : "Hotel not included";
  }

  render() {
    const { travel, seller } = this.state;

    // Display nothing if the data from the API server isn't fetched yet
    if (Object.keys(travel).length === 0 || Object.keys(seller).length === 0) {
      return null;
    }

    return (
      <div>
        <MainNav />
        <Wrap>
          <Grid wrap>
            <Col padded large="8" medium="8" small="12">
              <div className="card">
                <div className="card-image img">
                  <img src={`${API_URL}/img/${travel.image}`} alt="" />
                </div>
                <h2>{travel.name}</h2>
                <p> {travel.description}</p>
                <p className="price"> {travel.price} <span className="currency">kr</span> </p>
                <hr className="card-hr" />
                <p className="card-info"><Roundtrip color="#999" /> {this.renderRoundtripText(travel.roundtrip)}</p>
                <p className="card-info"><Building color="#999" /> {this.renderHotelText(travel.hotel)}</p>
              </div>
            </Col>

            <Col padded large="4" medium="4" small="12">
              <div className="card">
                <h2>Contact information</h2>
                <div>
                  <img src={`${API_URL}/img/${seller.image}`} alt="" className="card-seller-image" />
                </div>
                <p><strong>Name:</strong> {seller.first_name} {seller.last_name}</p>
                <p><strong>Email:</strong> {seller.email}</p>
                <p><strong>Rate:</strong> {seller.rating}</p>
              </div>
            </Col>
          </Grid>
        </Wrap>
      </div>
    );
  }
}

export default TravelDetails;
