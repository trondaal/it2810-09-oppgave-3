import React, { Component } from 'react';
import cookie from 'react-cookie';
import { Link } from 'react-router';
import api from '../../api';
import { API_URL } from '../../constants';
import './style.css';
import MainNav from '../../components/MainNav';
import Wrap from '../../components/Wrap';
import Button from '../../components/Button';
import StarRating from '../../components/StarRating';
var jwtDecode = require('jwt-decode');

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      travels: [],
      token: cookie.load('token'),
      loggedIn: false,
      showImageForm: false,
      showUploadButton: false,
      fileMessage: 'Choose a file...'
    };
    this.onLogout = this.onLogout.bind(this);
    this.uploadImage = this.uploadImage.bind(this);
    this.onImageSelected = this.onImageSelected.bind(this);
    this.confirmUpload = this.confirmUpload.bind(this);
  }

  componentDidMount() {
    if (this.state.token != null) {
      var decoded = jwtDecode(this.state.token);
      var time = new Date() / 1000;
      if (time > decoded.exp) {
        this.onLogout()
        cookie.remove('token');
        window.location.href = '/';
      }
      else {
        decoded = decoded._doc;
        var person = [{
          id: decoded.id,
          user_name: decoded.user_name,
          first_name: decoded.first_name,
          last_name: decoded.last_name,
          email: decoded.email,
          gender: decoded.gender,
          rating: decoded.rating,
          image: decoded.image,
          password: decoded.password,
          admin: decoded.admin,
        }];
        this.setState({loggedIn: person});
        api.getTravelBySeller(person[0].id)
          .then(travels => {this.setState({ travels })});
      }
    }
    else {
      window.location.href = '/';
    }
  }

  onLogout() {
    cookie.remove('token');
    this.setState({token: null, loggedIn: false});
  }

  uploadImage() {
    this.setState({showImageForm: true});
  }

  onImageSelected(e){
    e.preventDefault();
    var fileName = '';
    fileName = e.target.value.split( '\\' ).pop();
    if (fileName.length > 13) {
      fileName = fileName.substring(0, 12) + '...';
    }
    if( fileName )
      this.setState({
        fileMessage: fileName,
        showUploadButton: true
      });
  }

  confirmUpload(e) {
    e.preventDefault();
    api.uploadImage(this.state.loggedIn[0].id, document.getElementById('file-image').files[0])
          .then(res => {
                if (res.success) {
                }
                else {
                    this.setState({errorMessage: res.message});
                }
            });
  }

  render() {
    const user = this.state.loggedIn;
    const { travels} = this.state;
    return (
        <div>
          <MainNav user={this.state.loggedIn} onLogout={this.onLogout}/>
          <Wrap>
            {user &&
              <div className="my-profile">
                  <div className="my-image">
                    <img src={`${API_URL}/img/${user[0].image}`} alt="" />
                    <button className={this.state.showImageForm ? 'image-form' : 'success button '} onClick={this.uploadImage}>
                      <p>Change profile picture!</p>
                    </button>
                    <form className={this.state.showImageForm ? '' : 'image-form'} encType="multipart/form-data"  >
                      <input accept="image/*" type="file" id="file-image" className="input-image" name="image" onChange={this.onImageSelected} />
                      <label htmlFor="file-image">{this.state.fileMessage}</label>
                      <button className={this.state.showUploadButton ? 'button upload-button primary' : 'image-form'} onClick={this.confirmUpload}>
                        <p>Upload!</p>
                      </button>
                    </form>
                  </div>
                  <div className="my-info">
                    <div className="my-name">
                      {user[0].first_name} {user[0].last_name}
                    </div>
                    <div className="info-item my-username">
                      <strong>Username</strong>
                       <div>{user[0].user_name}</div>
                    </div>
                    <div className="info-item my-email">
                      <strong>Email</strong>
                       <div>{user[0].email}</div>
                    </div>
                    <div className="info-item my-rating">
                      <strong>Rating</strong>
                      <StarRating rating={user[0].rating} />{user[0].rating}
                    </div>
                    <div className="info-item my-gender">
                      <strong>Gender</strong>
                      <div>{user[0].gender}</div>
                    </div>

                  </div>
                  <div className="my-travels">
                    <p className="my-travels-title">My listed tickets <Button link="/travels/new"primary large>+</Button></p>
                    {/*travels.map(function(object, i ) {
                        return <Link to={`travels/${object.id}`}> {object.name} - {object.price} kr</Link>
                    })*/}
                    <table className="selectable">
                      <thead>
                      <tr>
                        <th>Destination</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Other</th>
                      </tr>
                      </thead>
                      <tbody>
                      {travels.map(travel => (
                          <tr key={travel.id}>
                            <td><Link to={`travels/${travel.id}`}>{travel.name}</Link></td>
                            <td><Link to={`travels/${travel.id}`}>{travel.price} kr</Link></td>
                            <td><Link to={`travels/${travel.id}`}>Tur/retur</Link></td>
                            <td><Link to={`travels/${travel.id}`}>Hotell inkludert</Link></td>
                          </tr>
                      ))}
                      </tbody>
                    </table>
                  </div>
              </div>
            }

          </Wrap>
        </div>
    );
  }
}

export default Profile;
