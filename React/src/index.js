import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router'

import Routes from './routes';

import './index.css';

/*
This is the entry point to the whole React project. It basically does 2 things:

- Load the global CSS (CSS that's used on every page)
- Load the `routes.js` component

*/
ReactDOM.render(
  <Routes history={browserHistory} />,
  document.getElementById('root')
);
