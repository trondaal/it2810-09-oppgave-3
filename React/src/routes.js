import React from 'react';
import { Router, Route } from 'react-router'

import Home from './pages/Home';
import TravelDetails from './pages/TravelDetails';
import Profile from './pages/Profile';
import NewTravel from './pages/NewTravel';
import NotFound from './pages/NotFound';


/*
`routes.js` decides what should happen when a user visits a URL:

 * `/` => Load the `pages/Home` component
 * `/travels/1` => Load the `pages/TravelDetails` component
 * `/rickroll` => Page does not exist. Display a 404 (the `pages/NotFound` component)

 */
const Routes = (props) => (
  <Router {...props}>
    <Route path="/" component={Home} />
    <Route path="/map" component={Home} />
    <Route path="/list" component={Home} />
    <Route path="/travels/new" component={NewTravel} />
    <Route path="/travels/:travelID" component={TravelDetails} />
    <Route path="/profile" component={Profile} />
    <Route path="*" component={NotFound} />
  </Router>
);
export default Routes;
