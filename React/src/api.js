/*
All API calls are defined in this file. It consists of several importable functions.

Example: Say that you want to get a list of "all travels". Then you'd do the following:
    ```
    import api from './api.js';

    api.getTravels()
      .then(travels => console.log(travels));
    ```
*/
import cookie from 'react-cookie';
import { API_URL } from './constants';


function getTravels() {
  return fetch(`${API_URL}/travels`)
    .then((response) => response.json());
}

function getTravel(id) {
  return fetch(`${API_URL}/travels/${id}`)
    .then((response) => response.json());
}

function createTravel(params, image) {
  var formData = new FormData();
  formData.append('image',image);
  formData.append('token', cookie.load('token'))
  for (var key in params) {
    formData.append(key, params[key]);
  }
  return fetch(`${API_URL}/travels`,{
    method: 'POST',
    body: formData,
  })
  .then((response) => response.json());
}

function getTravelBySeller(id) {
  return fetch(`${API_URL}/travels/s/${id}`)
    .then((response) => response.json());
}

function createUser(params) {
   return fetch(`${API_URL}/users`,{
      method: 'POST',
      body: JSON.stringify(params),
    })
    .then((response) => response.json());
}

function uploadImage(id, image) {
    var formData = new FormData();
    formData.append('image',image);
    formData.append('token', cookie.load('token'))
   return fetch(`${API_URL}/users/img/` + id,{
      method: 'PUT',
      body: formData,
    })
    .then((response) => response.json());
}

function getSellers() {
  return fetch(`${API_URL}/users`)
    .then((response) => response.json());
}

function getSeller(id) {
  return fetch(`${API_URL}/users/${id}`)
    .then((response) => response.json());
}

function login(params) {
  return fetch(`${API_URL}/authenticate`,{
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(params),
    })
    .then((response) => response.json());
}
export default {
  getTravels,
  getTravel,
  getSeller,
  getSellers,
  getTravelBySeller,
  createUser,
  uploadImage,
  login,
  createTravel,
};
