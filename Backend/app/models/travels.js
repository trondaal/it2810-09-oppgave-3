var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TravelSchema   = new Schema({
    id: Number,
    sellerID: Number,
    name: String,
    description: String,
    price: String,
    origin: String,
    hotel: Boolean,
    roundtrip: Boolean,
    image: String,
    destination_lat: String,
    destination_lng: String
});

module.exports = mongoose.model('travels', TravelSchema);