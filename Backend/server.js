// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var jwt        = require('jsonwebtoken');
var morgan     = require('morgan');
var fs         = require('fs');
var path       = require('path');
var config     = require('./config');
var User       = require('./app/models/user');
var Travels    = require('./app/models/travels');
var multer     = require('multer');
var storage    = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    var ext = require('path').extname(file.originalname);
    ext = ext.length>1 ? ext : "." + require('mime').extension(file.mimetype);
    require('crypto').pseudoRandomBytes(16, function (err, raw) {
      cb(null, (err ? undefined : raw.toString('hex') ) + ext);
    });
  }
})
mongoose.connect(config.database); // Connect to database, path defined in config file

// Configure app to use various plugins, etc,
// This allows us to use POST
var upload = multer({ storage: storage});
app.set('it2810secret', config.secret);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.static('uploads'));
var allowCrossDomain = function(req, res, next) {
  //Allow all origins to use API. Necessary for local development.
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
}
app.use(allowCrossDomain);
var port = process.env.PORT || 3001;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


// Initial route with some info (accessed at GET http://it2810-09.idi.ntnu.no:3001/api)
router.get('/', function(req, res) {
  res.json({ message: 'For list of users, use /api/users/ \nFor travels, use /api/travels' });   
});


//-------------------------------------------------------------------------------------------
// CHECK LOGIN AND CREATE AUTHENTICATION TOKEN
router.route('/authenticate')
.post(function(req, res){
  User.findOne({
    user_name: req.body.user_name
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {
      // Check if recieved password matches stored
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (err) {
          throw err;
        } 
        if (!isMatch) {
          res.json({ success: false, message: 'Authentication failed. Wrong password.' });
        }
        else {
          // Create a token for user. User info is encoded within token, can then be decoded on front end to check which user it is
          var token = jwt.sign(user, app.get('it2810secret'), {
            expiresIn : 60*60*24 // Expires after 24 hours
          });
          // Return token as JSON
          res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
          });

        }
      });
    }
  })
});




//---------------------------------------------------------------------------------------
// GET IMAGE BY URL
router.route('/img/:img_id')
.get(function(req,res) {
  if (req.params.img_id != null) {
    fs.stat(path.join(__dirname, '/uploads/', req.params.img_id), (err, stats) => {
      if (err == null) { 
        res.sendFile(path.join(__dirname, '/uploads/', req.params.img_id));
      } else if(err.code == 'ENOENT') {
        // File does not exist
        fs.writeFile('log.txt', 'File not found: ' + req.params.img_id);
      } else {
        console.log('Some other error: ', err.code);
      }
  });
  }
  else {res.close()}
});


// -----------------------------------------------------------------------------------
// CREATE NEW USER OR GET ALL
router.route('/users')
// create a user (accessed at POST http://it2810-09.idi.ntnu.no:3001/api/users)
.post(function(req, res) {        
  User.findOne({'user_name': req.body.user_name}, function(err, eUser) {
    if (err)
      res.send(err);
    if (eUser) {
      res.json({ success: false, message: 'A user with that username already exists.' });
    }
    else {
      User.count({}, function(err, count) {
        if (err)
          res.send(err);
        var user = new User();
        user.id = count+1;
        user.user_name = req.body.user_name;
        user.first_name = req.body.first_name;
        user.last_name = req.body.last_name;
        user.email = req.body.email;
        user.gender = req.body.gender;
        user.rating = 4;
        user.image = 'avatar.svg';
        user.password = req.body.password;
        user.admin = false;
        // save the user and check for errors
        user.save(function(err) {
          if (err)
            res.send(err);

          res.json({ success: true, message: 'User created!' });
        });
      }) 
    }
  });
})

// get all the users (accessed at GET http://it2810-09.idi.ntnu.no:3001/api/users)
.get(function(req, res) {
  User.find(function(err, users) {
    if (err)
      res.send(err);

    res.json(users);
  });
});

// ----------------------------------------------------------------------------------------
// GET SPECIFIC USER
router.route('/users/:user_id')
// get the user with that id (accessed at GET http://it2810-09.idi.ntnu.no:3001/api/user/:user_id)
.get(function(req, res) {
  User.findOne({'id':req.params.user_id}, function(err, user) {
  if (err)
    res.send(err);

  res.json(user);
  });
})


//----------------------------------------------------------------------------------------
//GET ALL TRAVLES
router.route('/travels')
// get all the travels (accessed at GET http://it2810-09.idi.ntnu.no:3001/api/travel)
.get(function(req, res) {
  Travels.find(function(err, travels) {
    if (err)
      res.send(err);

    res.json(travels);
  });
});

// ------------------------------------------------------------------------------------------
// GET SPECIFIC TRAVEL
router.route('/travels/:travels_id')
// get the travel with that id (accessed at GET http://it2810-09.idi.ntnu.no:3001/api/travels/:travels_id)
.get(function(req, res) {
  Travels.findOne({'id': req.params.travels_id}, function(err, travel) {
    if (err)
      res.send(err);

    res.json(travel);
  });
})

//------------------------------------------------------------------------------------------
// GET TRAVELS PUBLISHED BY A SELLER
router.route('/travels/s/:seller_id')
// get the travel with that id (accessed at GET http://it2810-09.idi.ntnu.no:3001/api/travels/:travels_id)
.get(function(req, res) {
  Travels.find({'sellerID': req.params.seller_id}, function(err, travels) {
    if (err)
      res.send(err);

    res.json(travels);
  });
})






// ===========================================================================
// ================ AUTHENTICATION CHECK ==================================
// ===========================================================================
// Every route after this requires a JWT to pass 

router.use(function(req,res,next) {
  // Check for token in request
  var token = req.body.token;

  if (token) {

    // Verify secret and check if it has expired
    jwt.verify(token, app.get('it2810secret'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // Everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
      success: false, 
      message: 'No token provided.' 
    });

  }
});

//------------------------------------------------------------------
//CREATE NEW TRAVEL
router.route('/travels')
// create a travel (accessed at POST http://it2810-09.idi.ntnu.no:3001/api/travels)
.post(upload.single('image'), function(req, res) {
  Travels.count({}, function(err, count) {
    if (err)
      res.send(err);

    var travel = new Travels();      // create a new instance of the Travels model
    travel.id = count+1;  // update the travels info
    travel.sellerID = req.body.sellerID;
    travel.name = req.body.name;
    travel.description = req.body.description;
    travel.price = req.body.price;
    if (req.file) {
      travel.image = req.file.filename;
    }
    travel.hotel = req.body.hotel;
    travel.roundtrip = req.body.roundtrip;
    travel.destination_lat = req.body.destination_lat;
    travel.destination_lng = req.body.destination_lng;
    travel.save(function(err) {
      if (err)
        res.send(err);

      res.json({success: true, message: 'Travel created!' });
    });
  })
})


// ----------------------------------------------------------------------------------------
// UPDATE OR DELETE USER
router.route('/users/:user_id')
// update the user with this id (accessed at PUT http://it2810-09.idi.ntnu.no:3001/api/user/:user_id)
.put(function(req, res) {
  // use our travels model to find the travel we want
  User.findOne({'id':req.params.user_id}, function(err, user) {
    if (err)
      res.send(err);

    if (req.body.user_name) {
      user.user_name = req.body.user_name;
    }
    if (req.body.first_name) {
      user.first_name = req.body.first_name;
    }
    if (req.body.last_name) {
      user.last_name = req.body.last_name;
    }
    if (req.body.email) {
      user.email = req.body.email;
    }
    if (req.body.gender) {
      user.gender = req.body.gender;
    }
    if (req.body.rating) {
      user.rating = req.body.rating;
    }
    if (req.body.image) {
      user.image = req.body.image;
    }
    if (req.body.password) {
      user.password = req.body.password;
    }
    // save the updatet user
    user.save(function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'User updated!' });
    });

  });
})

// delete the user with this id (accessed at DELETE http://it2810-09.idi.ntnu.no:3001/api/travels/:travels_id)
.delete(function(req, res) {
  User.remove({
    'id': req.params.user_id
  }, function(err, travel) {
    if (err)
      res.send(err);

    res.json({ message: 'Successfully deleted' });
  });
});

//----------------------------------------------------------------------------------
// UPDATE PROFILE PICTURE
router.route('/users/img/:user_id')
// Update the profile picture of the user with this id (accessed at PUT http://it2810-09.idi.ntnu.no:3001/api/users/img/:user_id)
.put(upload.single('image'), function(req, res) {
  // use our User model to find the user we want
  User.findOne({'id':req.params.user_id}, function(err, user) {
    if (err)
      res.send(err);

    user.image = req.file.filename;
    user.save(function(err) {
      if (err)
        res.send(err);

      res.json({success: true, img_url: req.file.filename, message: 'User updated!' });
    });
  });
})


// --------------------------------------------------------------------------------------------
// UPDATE OR DELETE TRAVEL
router.route('/travels/:travels_id')
// update the travel with this id (accessed at PUT http://it2810-09.idi.ntnu.no:3001/api/travels/:travels_id)
.put(function(req, res) {
  // use our travels model to find the travel we want
  Travels.findOne({'id': req.params.travels_id}, function(err, travel) {
    if (err)
      res.send(err);

    if (req.body.name) {
      travel.name = req.body.name;
    }
    if (req.body.description) {
      travel.description = req.body.description;
    }
    if (req.body.price) {
      travel.price = req.body.price;
    }
    if (req.body.hotel) {
      travel.hotel = req.body.hotel;
    }
    if (req.body.roundtrip) {
      travel.roundtrip = req.body.roundtrip;
    }
    if(req.body.destination_lat){
      travel.destination_lat = req.body.destination_lat;
    }
    if(req.body.destination_lng){
      travel.destination_lng = req.body.destination_lng;
    }
    travel.save(function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'Travel updated!' });
    });
  });
})

// delete the travel with this id (accessed at DELETE http://it2810-09.idi.ntnu.no:3001/api/travels/:travels_id)
.delete(function(req, res) {
  Travels.remove({
    'id': req.params.travels_id
  }, function(err, travel) {
    if (err)
      res.send(err);

    res.json({ message: 'Successfully deleted' });
  });
});



//----------------------------------------------
// All routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('API server running on ' + port);