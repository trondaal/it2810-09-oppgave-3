# Kom i gang
### Ting du må gjøre *første* gang
1. Last ned og installér **node** fra [nodejs.org](https://nodejs.org/en/)
2. Åpne en **terminal** og navigér til `<root>/React`, hvor `<root>` er rotmappen til prosjektet.
3. Kjør kommandoen `npm install`
    - Dette vil installere alle prosjektets "dependencies", som **React** og **React Router**.
4. *(Ekstra):* Vi kjører en ekstern backend-server som webapplikasjonen vår sender sine forespørsler til. Dersom det er ønskelig å kjøre en egen backend-server kan feltet `API_URL` i filen `<root>/React/src/constants.js` endres.



### Ting du må gjøre *hver gang*
1. Åpne et terminalvindu og navigér til `<root>/React`
2. Kjør følgende kommando:

    - `npm start`
        - Starter serveren som kjører webapplikasjonen

3. Ferdig! Du kan nå besøke http://localhost:3000 i nettleseren din og kode i vei!

# Konsept: Ticket resale (finn.no for reiser)
![bilde av forsiden](https://cloud.githubusercontent.com/assets/3471625/20246522/aff57fa4-a9b8-11e6-8fb1-c194c352f9f7.png)
![listevisning](https://cloud.githubusercontent.com/assets/3471625/20246525/aff5be60-a9b8-11e6-9ab5-10486b50ee02.png)
![kartvisning](https://cloud.githubusercontent.com/assets/3471625/20246523/aff5a934-a9b8-11e6-81b0-4931ae04f0c1.png)
![profilside](https://cloud.githubusercontent.com/assets/3471625/20246524/aff584e0-a9b8-11e6-8550-f9f15df14c12.png)

### Lyst til å ta en titt?
Nettsiden kan besøkes på URL-en `http://it2810-09.idi.ntnu.no`. Hvis du ønsker kan du bruke testbrukeren vår for å logge inn:

**Brukernavn:** `CuteGirl`
**Passord:** `password123`

Det er også mulig å lage sin egen bruker.


# Om filstrukturen (Frontend)
> «Hva er greia med alle disse filene?!»

Håpet med filstukturen vi har valgt er at den vil skalere godt. Nedenfor vil vi forklare hva som er tanken
bak de forskjellige mappene og filene.

### Overblikk av filstrukturen

Merk at mappen `node_modules` ikke er med i denne oversikten, eller at ingen filer er listet etter nivå 2 (hvor `<root>` er nivå 0).

Mapper og filer er henholdsvis merket med `/` og `*` etter navnet. `<root>` vises som `.`.

```
.
├── public/
│   ├── favicon.ico*
│   └── index.html*
├── src/
│   ├── components/
│   │   ├── Button/
│   │   ├── ButtonGroup/
│   │   ├── Col/
│   │   ├── Grid/
│   │   ├── Input/
│   │   ├── LoginButton/
│   │   ├── LoginForm/
│   │   ├── MainNav/
│   │   ├── Select/
│   │   ├── StarRating/
│   │   ├── SVG/
│   │   └── Wrap/
│   ├── pages/
│   │   ├── Home/
│   │   ├── NotFound/
│   │   ├── Profile/
│   │   └── TravelDetails/
│   ├── api.js*
│   ├── constants.js*
│   ├── index.css*
│   ├── index.js*
│   └── routes.js*
└── package.json*
```

#### Forsiden består av følgende komponenter:
![component structure](https://cloud.githubusercontent.com/assets/3471625/20246747/253a9754-a9be-11e6-8b15-492f8b31074b.png)

### public/
Denne mappen ble generert av **Create React App** (forklart senere) og blir knapt rørt av oss. `public/index.html` er inngangsporten til hele prosjektet.

### src/components/
I denne mappen legges alle React-komponenter vi planlegger å **gjenbruke**. Eksempler på slike komponenter kan være...

* Button
* ButtonGroup
* Col
* Grid
* Input
* LoginButton
* LoginForm
* MainNav
* Select
* StarRating
* SVG
* Wrap

### src/pages/
Alle undermapper her representerer en **side**. Eksempler på sider kan f.eks. være...

* Home (forsiden)
* Profile (profilsiden til en bruker)
* TravelDetails (mer om en spesifikk reise)
* NewTravel (oprette ny reise objekt)
* NotFound (404-side)

### index.js
Dette er inngangsporten til React-prosjektet. Den gjør (for øyeblikket) to ting:

* Laster inn CSS som skal gjelde for hele nettsiden (fra `index.css`)
* Gir kontrollen videre til `routes.js` (se neste avsnitt)

### routes.js
I `routes.js` bestemmer vi hva som skal skje når en bruker besøker en URL.

**Eksempel:**

* `/` => Last komponenten `pages/Home`
* `/travels/1` => Last komponenten `pages/TravelDetails` med innhold fra reise med `id: 1`
* `/rickroll` => Siden finnes ikke. Last inn `pages/NotFound`
  - "`/rickroll`" kan være hvilken som helst URL. Den blir her bare brukt som et eksempel på en komponent som ikke eksisterer.

### api.js
Alle API-kall er definert i denne filen. Den består av masse forskjellige importerbare funksjoner.

**Eksempel:** Si at du ønsker å skaffe en liste over **alle reiser** fra serveren. Da kan du gjøre følgende:

```
import api from './api.js';

api.getTravels()
  .then(travels => console.log(travels));
```

#### ***Noen ekstra detaljer:***
* Når *Login*-knappen klikkes på dukker en ekstra komponent opp: *LoginForm*.
* Når en bruker har logget inn vil *Login*-knappen (som egentlig er en egen *LoginButton*-komponent) byttes ut med en *ButtonGroup* som har tre underliggende *Button*-komponenter:
    * Den første viser profilbildet, og viser/skjuler de to neste knappene.
    * Knapp nummer to linker til profilsiden (`pages/Profile`).
    * Den siste knappen logger brukeren ut av webapplikasjonen.

# Backend
Vår frontend blir servert data av et RESTful API. Det er en NodeJS server med Express som har en mongoDB i bunnen. 

### Express

Express servere tar imot en URL og sender tilbake riktig data. 
For eksempel, `http://it2810-09.idi.ntnu.no:3001/api/travels` vil gi tilbake JSON data: 
```
{
(...)
"description":"Implemented administration challange",
"destination_lat":"28.2","destination_lng":"83.98166700000002",
"hotel":false,
"id":15,
"image":"09bff937766e667b42b6d86bbb183276.jpg",
"name":"Nepal",
"price":"611",
"roundtrip":true,
"sellerID":6
}
``` 
osv.

Serveren bruker JWT (JSON Web Token) for å autentisere brukerne. Tokenen blir encoded med informasjon om brukeren og har en levetid på 24 timer. 
Hvis en bruker prøver å bruke adgangsbegrensede funksjoner uten gyldig token vil en relog være nødvendig for å fornye den. 
Tokene er sendt sammen med alle requests til serveren som trenger den ved å piggybacke på bodien til requesten. 

Serveren har CRUD(create, read, update, delete) funksjonalitet gjennom routing og håndtering av forskjellig request metoder. 
Express serveren har følgende routing:

* /travels  og  /users
    * get: Få alle lagrede objecter
* /travels/*  og  /users/*
    * get: Få spesifikk object med id = *
* /travels/s/*
    * get: Få alle travel objekt publisert av bruker med id = *
* /users 
    * post: Lag ny bruker
* /authenticate
    * post: Login funksjonen. Sjekker brukernavn og passord, lager JWT token hvis OK. 
* /img/*
    * get: Få spesifikk bilde med navn = *

Visse Express routinger på sereren krever gyldig JWT token i request for å fullføres:

* /travels
    * post: Lag ny travel objekt
* /users/*  og  /travels/*
    * put: Oppdatere object
    * delete: Slette object med id = *
* /users/img/*
    * put: Oppdatere profilbilde til bruker med id = *

Alle passord er hashet og saltet når de lagres i databasen. 
### Overblikk av filstrukturen

Merk at mappen `node_modules` ikke er med i denne oversikten.

Mapper og filer er henholdsvis merket med `/` og `*` etter navnet. `<root>` vises som `.`.

```
.
├── uploads/
├── app/
│   └── models/
│       ├── travels.js*
│       ├── user.js*
├── config.js*
├── log.txt*
├── server.js*
├── package.json*
```

Bilder som blir lastet opp blir plassert i uploads mappen med et unikt random generert navn.

### Linux server
Serveren bruker Linux screen's for å kjøre forskjellige processer samtidig. 
Den kjører nå screen's:

* React-app: Kjører frontende som kan nåes på `http://it2810-09.idi.ntnu.no`
* Backend API: Kjører api som tar imot requests fra React-appen. Kan nåes på `http://it2810-09.idi.ntnu.no:3001/api/`
* mongoDB: Kjører mongoDB databasen som tar imot fra backend APIet

## NB: 
Den eksterne backend serveren tar ikke imot requests fra andre kilder enn `http://it2810-09.idi.ntnu.no`, så den kan ikke brukes til lokal utvikling.

Dette er gjort ved å sette `Access-Control-Allow-Origin` til nevnt kilde istedenfor `*`, som gir alle tilgang. 

# Ting man får med på kjøpet ved å bruke `Create React App`
Dette prosjektet baserer seg på [Create React App](https://github.com/facebookincubator/create-react-app). Årsaken til at vi gjør det er for å unngå å bruke MASSE tid på å konfigurere et utviklermiljø som bare blir *«helt OK»*.

Ved å bruke **Create React App** får vi følgende «out of the box»:

* [webpack](https://webpack.github.io/) with [webpack-dev-server](https://github.com/webpack/webpack-dev-server), [html-webpack-plugin](https://github.com/ampedandwired/html-webpack-plugin) and [style-loader](https://github.com/webpack/style-loader)
* [Babel](http://babeljs.io/) with ES6 and extensions used by Facebook (JSX, [object spread](https://github.com/sebmarkbage/ecmascript-rest-spread/commits/master), [class properties](https://github.com/jeffmo/es-class-public-fields))
* [Autoprefixer](https://github.com/postcss/autoprefixer)
* [ESLint](http://eslint.org/)
* and others.

Hvis det er noe du ikke skjønner hvordan henger sammen, kan det derfor være greit å ta en kjapp titt på
[dokumentasjonen til Create React App](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
